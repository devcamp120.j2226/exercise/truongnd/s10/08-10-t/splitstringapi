package com.devcamp.spiltstringapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpiltstringapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpiltstringapiApplication.class, args);
	}

}
